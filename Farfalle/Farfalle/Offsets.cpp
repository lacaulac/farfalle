#include "stdafx.h"
#include "Offsets.h"

//Mandatory for the static shite
DWORD COffset::LocalPlayer;
DWORD COffset::EntityList;
DWORD COffset::ClientState;

//ClientState shite
DWORD COffset::m_nDeltaTick;

//CPlayer shite
DWORD COffset::m_hMyWeapons;
DWORD COffset::m_vecOrigin;
DWORD COffset::m_vecViewOffset;
DWORD COffset::m_dwBoneMatrix;
DWORD COffset::m_aimPunchAngle;
DWORD COffset::m_iHealth;
DWORD COffset::m_iTeamNum;
DWORD COffset::m_iCrosshairId;

COffset::COffset()
{
}

void COffset::GetOffsets()
{
	LocalPlayer = Offsets::LocalPlayer;
	EntityList = Offsets::EntityList;
	ClientState = Offsets::ClientState;
	
	m_nDeltaTick = Offsets::m_nDeltaTick;

	m_hMyWeapons = Offsets::m_hMyWeapons;
	m_vecOrigin = Offsets::m_vecOrigin;
	m_vecViewOffset = Offsets::m_vecViewOffset;
	m_dwBoneMatrix = Offsets::m_dwBoneMatrix;
	m_aimPunchAngle = Offsets::m_aimPunchAngle;
	m_iHealth = Offsets::m_iHealth;
	m_iTeamNum = Offsets::m_iTeamNum;
	m_iCrosshairId = Offsets::m_iCrosshairId;
}
