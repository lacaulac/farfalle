// Farfalle.cpp : définit les fonctions exportées pour l'application DLL.
//

#include "stdafx.h"
#include "Farfalle.h"
#include "SDK.h"
#include "Hooks.h"
#include "InterfaceManager.hpp"
#include "ConfigManager.h"
#include "VMTHook.h"

#define VERSION "Alpha 1.0"

DWORD ClientDLL;
DWORD EngineDLL;

IVEngineClient* Engie;
IClientEntityList* Enties;

CreateMove_t oCreateMove;

void MainThread()
{
	printf("Welcome to Farfalle %s! :D\n", VERSION);
	ConfigManager::GetConfig();
	COffset::GetOffsets();
	//Load config file
	//Delete config file
	//EVERYBODY DO TEH HUKS
	InterfaceManager EngineMGR("engine.dll");
	Engie = (IVEngineClient*)EngineMGR.getInterface("VEngineClient014");
	InterfaceManager ClientMGR("client.dll");
	Enties = (IClientEntityList*)ClientMGR.getInterface("VClientEntityList003");
	void* BaseClientDLL = (void *)(ClientMGR.getInterface("VClient018"));
	void** BaseClientDLLVMT = *(void***)BaseClientDLL;
	DWORD ClientMode_t = *(DWORD*)((DWORD)BaseClientDLLVMT[10] + 5);
	DWORD ClientMode = *(DWORD*)(ClientMode_t);
	VMTHook ClientModeHook(ClientMode);
	oCreateMove = (CreateMove_t)ClientModeHook.HookMethod((DWORD)&hkCreateMove, 24);
	while (true)
	{
		if (GetAsyncKeyState('L'))
		{
			Engie->DebugInfo();
			while (GetAsyncKeyState('L'))
			{
				Sleep(200);
			}
		}
		Sleep(200);
	}
}
