#pragma once

class AimSettings
{
public:
	float radius = 20.0f; //W2S distance threshold
	float smooth = 0.0f; //Coefficent to apply to the lerp (smooth of 1.0f will not move the crosshair at all)
	float aimChance = 0.0f; //Chances of triggering the aimbot (0.0f - 1.0f) on a specific shot
	float notHeadChance = 0.0f; //Chances of triggering the aimbot(0.0f - 1.0f) if not going for the headshot/neckshot
	int shots = 0; //Number of shots to use the aimbot on

	AimSettings();
};

class TriggerSettings
{
public:
	int minDelay = 0; //Minimum delay (ms)
	int maxDelay = 1; //Maximum delay (ms)
	float triggerChance = 0.3f; //Chances of triggering the triggerbot (0.0f - 1.0f) on a specific shot
	float triggerWhenLeftChance = 0.3f; //Chances of triggering the triggerbot after the enemy left (0.0f - 1.0f) on a specific shot

	TriggerSettings();
};

class AASettings
{
public:
	float AAChance = 0.0f; //Chances of doing the legit AA

	AASettings();
};

class ConfigManager
{
public:
	static AimSettings Aim;
	static TriggerSettings Trigger;
	static AASettings AA;
	ConfigManager();
	static void GetConfig();
	static AimSettings* GetAimSettings();
	static TriggerSettings* GetTriggerSettings();
	static AASettings* GetAASettings();
	~ConfigManager();
};

