#include "stdafx.h"
#include "Utils.h"


void Utils::PrintToConsole(char * format, ...)
{
	//msg_t game_console_print;
	//game_console_print = reinterpret_cast< msg_t >(GetProcAddress(GetModuleHandleA("tier0.dll"), "Msg"));
	char buffer[255];
	va_list args;
	va_start(args, format);
	vsprintf(buffer, format, args);
	va_end(args);
	printf(buffer);
	//game_console_print(args);
}

Utils::Utils()
{
}


Utils::~Utils()
{
}
