#pragma once
#include "Vector.h"
#include "Offsets.h"

class Player
{
public:
	Player();
	Vector getBonePosition(int boneID);
	Vector getPosition();
	Vector getSightPosition();
	Vector getAimPunchAngles();
	int getHealth();
	int getTeam();
	int getPlayerInCrossID();
	~Player();
};
