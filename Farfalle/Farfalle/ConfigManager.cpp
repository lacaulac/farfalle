#include "stdafx.h"
#include "ConfigManager.h"

//Mandatory for the static shite
AimSettings ConfigManager::Aim;
TriggerSettings ConfigManager::Trigger;
AASettings ConfigManager::AA;

ConfigManager::ConfigManager()
{
}

void ConfigManager::GetConfig()
{
	//load the config file into LegitBot config structs
	//delete the temporary config file so the launcher can close
}

AimSettings * ConfigManager::GetAimSettings()
{
	return &Aim;
}

TriggerSettings * ConfigManager::GetTriggerSettings()
{
	return &Trigger;
}

AASettings * ConfigManager::GetAASettings()
{
	return &AA;
}


ConfigManager::~ConfigManager()
{
}

AimSettings::AimSettings()
{
}

TriggerSettings::TriggerSettings()
{
}

AASettings::AASettings()
{
}
