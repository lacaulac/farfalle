#pragma once

namespace Offsets
{
	const DWORD LocalPlayer = 0xAA6614;
	const DWORD EntityList = 0x4A8387C;
	const DWORD ClientState = 0x57D894;

	//ClientState shite
	const DWORD m_nDeltaTick = 0x0;

	//CPlayer shite
	const DWORD m_hMyWeapons = 0x2DE8;
	const DWORD m_vecOrigin = 0x134;
	const DWORD m_vecViewOffset = 0x104;
	const DWORD m_dwBoneMatrix = 0x2698;
	const DWORD m_aimPunchAngle = 0x301C;
	const DWORD m_iHealth = 0xFC;
	const DWORD m_iTeamNum = 0xF0;
	const DWORD m_iCrosshairId = 0xB2A4;
}

class COffset
{
public:
	static DWORD LocalPlayer;
	static DWORD EntityList;
	static DWORD ClientState;

	//ClientState shite
	static DWORD m_nDeltaTick;

	//CPlayer shite
	static DWORD m_hMyWeapons;
	static DWORD m_vecOrigin;
	static DWORD m_vecViewOffset;
	static DWORD m_dwBoneMatrix;
	static DWORD m_aimPunchAngle;
	static DWORD m_iHealth;
	static DWORD m_iTeamNum;
	static DWORD m_iCrosshairId;

public:
	COffset();
	static void GetOffsets();
	//signature is the sig ("AEFFB34232DEXXXXAEBE3214134")
	//beginning is the number of characters before the offset we're retrieving
	//length is the length of the offset(should be 4 bytes, so 8(+1\0)chars)
	static DWORD GetOffset(char* signature, int beginning, int length=8);
};