#pragma once
using msg_t = void(__cdecl*)(const char*, ...);

class Utils
{
public:
	static void PrintToConsole(char* format, ...);
	Utils();
	~Utils();
};

