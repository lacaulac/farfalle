#include "stdafx.h"
#include "Hooks.h"
#include "SDK.h"
#include "LegitBot.h"


bool __fastcall hkCreateMove(void * _this, int edx, float frameTime, CUserCmd * pCmd)
{
	Vector oldAngles = pCmd->viewangles;
	oCreateMove(_this, frameTime, pCmd);
	//Player* LocalPlayer = (Player*)Engie->GetLocalPlayer();
	Player* LocalPlayer = Enties->GetClientEntity(Engie->GetLocalPlayer());
	if (!(pCmd->buttons & IN_RELOAD))
	{
		LegitBot::DoTrigger(pCmd);
		if ((pCmd->buttons & IN_ATTACK))
		{
			LegitBot::DoAimbot(pCmd);
			LegitBot::ClampAngles(&pCmd->viewangles);
			return true; //Apply your angles
		}
	}
	else
	{
		LegitBot::DoLegitAA(pCmd, oldAngles);
		LegitBot::ClampAngles(&pCmd->viewangles);
		return false; //Don't apply your angles
	}

	/*
	//TODO Fix movement
	float oFMove = pCmd->forwardmove;
	float oSMove = pCmd->sidemove;

	float yawDelta = pCmd->viewangles.y - oldAngles.y;

	// Compensate silent aim
	//pCmd->forwardmove = (cos(DEG2RAD(yawDelta)) * oFMove) + (cos(DEG2RAD(yawDelta + 90.0f)) * oSMove);
	//pCmd->sidemove = (sin(DEG2RAD(yawDelta)) * oFMove) + (sin(DEG2RAD(yawDelta + 90.0f)) * oSMove);
	*/

	LegitBot::ClampAngles(&pCmd->viewangles);

	//return GetAsyncKeyState('E')?true:false;
	//true means that we change our drawn view angles
	return true;
}
