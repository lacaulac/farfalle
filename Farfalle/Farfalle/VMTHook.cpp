﻿#include "stdafx.h"
#include "VMTHook.h"
#include <cstdlib>


VMTHook::VMTHook()
{
}


VMTHook::~VMTHook()
{
}

VMTHook::VMTHook(DWORD p_adr)
{
	Init(p_adr);
}

void VMTHook::Init(DWORD p_adr)
{
	target = p_adr;
	oldVMTAddress = *(DWORD*)(target);
	maxVMTIndex = -1;
	while (!IsBadCodePtr((FARPROC)((DWORD*)oldVMTAddress)[maxVMTIndex + 1]))
	{
		maxVMTIndex++;
	}
	newVMTAddress = (DWORD)malloc(sizeof(DWORD) * (maxVMTIndex + 1)); //Allocation de la memoire
	memcpy((DWORD*)newVMTAddress, (DWORD*)oldVMTAddress, sizeof(DWORD) * (maxVMTIndex + 1));
	ReHook();
}

DWORD VMTHook::HookMethod(DWORD p_fn, int p_index)
{
	DWORD oldAdr = ((DWORD*)newVMTAddress)[p_index];
	((DWORD*)newVMTAddress)[p_index] = p_fn;
	return oldAdr;
}

DWORD VMTHook::GetOldMethod(int p_index)
{
	return ((DWORD*)oldVMTAddress)[p_index];
}

void VMTHook::UnHook()
{
	*(DWORD*)target = oldVMTAddress;
}

void VMTHook::ReHook()
{
	*(DWORD*)target = newVMTAddress;
}
