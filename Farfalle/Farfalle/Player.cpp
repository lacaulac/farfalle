#include "stdafx.h"
#include "Player.h"


Player::Player()
{
}

Vector Player::getBonePosition(int boneID)
{
	Vector tmp;
	DWORD BoneMatrix = *(DWORD*)(this + COffset::m_dwBoneMatrix);
	tmp.x = *(float*)(BoneMatrix + (0x30 * boneID) + 0x0C);
	tmp.y = *(float*)(BoneMatrix + (0x30 * boneID) + 0x1C);
	tmp.z = *(float*)(BoneMatrix + (0x30 * boneID) + 0x2C);
	return tmp;
}

Vector Player::getPosition()
{
	return *(Vector*)(this + COffset::m_vecOrigin);
}

Vector Player::getSightPosition()
{
	Vector tmp = getPosition();
	Vector tmp2 = *(Vector*)(this + COffset::m_vecViewOffset);
	tmp.x += tmp2.x;
	tmp.y += tmp2.y;
	tmp.z += tmp2.z;
	return tmp;
}

Vector Player::getAimPunchAngles()
{
	return *(Vector*)(this + COffset::m_aimPunchAngle);
}

int Player::getHealth()
{
	return *(int*)(this + COffset::m_iHealth);
}

int Player::getTeam()
{
	return *(int*)(this + COffset::m_iTeamNum);
}

int Player::getPlayerInCrossID()
{
	return *(int*)(this + COffset::m_iCrosshairId);
}


Player::~Player()
{
}
