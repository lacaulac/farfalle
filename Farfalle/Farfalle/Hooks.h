#pragma once
#include "SDK.h"

typedef bool(__thiscall *CreateMove_t)(void*, float frameTime, CUserCmd* pCmd);

bool __fastcall hkCreateMove(void* _this, int edx, float frameTime, CUserCmd* pCmd);

extern CreateMove_t oCreateMove;