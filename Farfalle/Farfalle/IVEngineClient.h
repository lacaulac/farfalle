#pragma once

#include "VMTHook.h"
#include <cstdint>
#include "virtuals.h"
#include "Utils.h"
#include "IClientEntityList.h"

constexpr auto MAX_PLAYER_NAME_LENGTH = 128;
constexpr auto SIGNED_GUID_LEN = 32;

typedef float matrix3x4[3][4];

typedef struct player_info_s
{
private:
	int64_t unknown;
public:
	union
	{
		int64_t xuid;

		struct
		{
			int xuid_low;
			int xuid_high;
		};
	};

	char name[MAX_PLAYER_NAME_LENGTH];
	int userid;
	int m_nUserID;
	char guid[SIGNED_GUID_LEN + 1];
	unsigned int friendsid;
	char friendsname[MAX_PLAYER_NAME_LENGTH];
	bool fakeplayer;
	bool ishltv;
	unsigned int customfiles[4];
	unsigned char filesdownloaded;
} player_info_t;

class IVEngineClient
{
private:
public:
	int GetLocalPlayer()
	{
		return GetVirtualFunction<int(__thiscall *)(IVEngineClient*)>(this, 12)(this);
		//return *(int*)(GetModuleHandleA("client.dll") + Offsets::LocalPlayer);
	}
	int GetScreenSize(int* width, int* height)
	{
		return GetVirtualFunction<int(__thiscall *)(IVEngineClient*, int*, int*)>(this, 5)(this, width, height);
	}
	matrix3x4& GetW2SMatrix()
	{
		return GetVirtualFunction<matrix3x4&(__thiscall *)(IVEngineClient*)>(this, 37)(this);
	}
	int GetPlayerForUserID(int userid)
	{
		return GetVirtualFunction<int(__thiscall *)(IVEngineClient*, int)>(this, 9)(this, userid);
	}
	void ClientCmd_Unrestricted(const char* command, bool delayed = false)
	{
		return GetVirtualFunction<void(__thiscall *)(IVEngineClient*, const char*, bool)>(this, 114)(this, command, delayed);
	}
	bool GetPlayerInfo(int index, player_info_t* player_info)
	{
		return GetVirtualFunction<bool(__thiscall *)(IVEngineClient*, int, player_info_t*)>(this, 8)(this, index, player_info);
	}
	bool IsConnected()
	{
		return GetVirtualFunction<bool(__thiscall *)(IVEngineClient*)>(this, 27)(this);
	}
	bool IsInGame()
	{
		return GetVirtualFunction<bool(__thiscall *)(IVEngineClient*)>(this, 26)(this);
	}
	void DebugInfo()
	{
		Utils::PrintToConsole("GetLocalPlayer: %p\n", this->GetLocalPlayer());
		Utils::PrintToConsole("GetClientEntity(GetLocalPlayer): %p\n", Enties->GetClientEntity(this->GetLocalPlayer()));
		int tmpW, tmpH;
		this->GetScreenSize(&tmpW, &tmpH);
		Utils::PrintToConsole("GetScreenSize: W|%d H|%d\n", tmpW, tmpH);
		Utils::PrintToConsole("GetPlayerForUserID(1): %p\n", this->GetPlayerForUserID(1));
		Utils::PrintToConsole("IsConnected: %s\n", (this->IsConnected() ? "true" : "false"));
		Utils::PrintToConsole("IsInGame: %s\n", (this->IsInGame() ? "true" : "false"));
		player_info_t tmpInfo;
		this->GetPlayerInfo(1, &tmpInfo);
		Utils::PrintToConsole("Player 1 info address: %p\n\n\n", &tmpInfo);
	}
};

extern IVEngineClient* Engie;