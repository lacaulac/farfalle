﻿#include "stdafx.h"
#include "LegitBot.h"

//Mandatory for the static shite
int LegitBot::AATicks;

LegitBot::LegitBot()
{
}

void LegitBot::InitLegitBot()
{
	AATicks = 0;
}

void LegitBot::DoAimbot(CUserCmd * pCmd)
{
	float distance = 0.0f;
	Player* LocalPlayer = Enties->GetClientEntity(Engie->GetLocalPlayer());
	Player* target = GetClosestPlayerToCrosshair(&distance);
	if (target && distance <= ConfigManager::GetAimSettings()->radius && LocalPlayer)
	{
		Vector targetPoint = GetPointToAimAt(target);
		Vector eyePosition = LocalPlayer->getSightPosition();
		Vector newAngle = CompensateRecoil(CalcAngles(eyePosition, targetPoint), LocalPlayer->getAimPunchAngles());

		ClampAngles(&newAngle);

		Vector currentAngle = pCmd->viewangles;
		Vector finalAngle = LerpAngles(currentAngle, newAngle, ConfigManager::GetAimSettings()->smooth);
		ClampAngles(&finalAngle);
		pCmd->viewangles = finalAngle;
		//Calculate angle to the target

		//Apply Recoil compensation
		//Apply lerp
		//Apply angles to pCmd
	}


	//How to use config => ConfigManager::GetAimSettings()->shots = 420;
}

void LegitBot::DoLegitAA(CUserCmd * pCmd, Vector oldView)
{
	//Do the AA using
	//Compensate the movement using old angles
}

void LegitBot::DoTrigger(CUserCmd * pCmd)
{
	Player* LocalPlayer = Enties->GetClientEntity(Engie->GetLocalPlayer());
	if (LocalPlayer)
	{
		Vector aimpunch = LocalPlayer->getAimPunchAngles();
		if (aimpunch.x == 0.0f && aimpunch.y == 0.0f && aimpunch.z == 0.0f)
		{
			int target = LocalPlayer->getPlayerInCrossID();
			if (target != 0 && target <= 64)
			{
				Player* targetEntity = Enties->GetClientEntity(target);
				//Generate a random number between 0.0f and 1.0f, check if it is under our threshold
				if (targetEntity && randomFloat() <= ConfigManager::GetTriggerSettings()->triggerChance || targetEntity->getTeam() != LocalPlayer->getTeam())
				{
					//Proceed to trigger within a generated range of timing
					Sleep(randomInt(ConfigManager::GetTriggerSettings()->minDelay, ConfigManager::GetTriggerSettings()->maxDelay));
					target = LocalPlayer->getPlayerInCrossID();
					//Generate chance of triggering after target left crosshair
					if (target || randomFloat() <= ConfigManager::GetTriggerSettings()->triggerWhenLeftChance)
					{
						pCmd->buttons |= IN_ATTACK;
					}
				}
			}
		}
	}
}

Vector LegitBot::CompensateRecoil(Vector pointToAccess, Vector aimPunch)
{
	//Prendre point à viser
	//Appliquer le recul
	//Bouger la vue vers le point avec recul pris en compte
	pointToAccess.x -= aimPunch.x * 2;
	pointToAccess.y -= aimPunch.y * 2;
	pointToAccess.z -= aimPunch.z * 2;
	return pointToAccess;
}

Vector LegitBot::CalcAngles(Vector src, Vector dst)
{
	Vector diff;
	diff.x = src.x - dst.x;
	diff.y = src.y - dst.y;
	diff.z = src.z - dst.z;
	Vector angles;
	float hypotenuse = sqrtf(diff.x * diff.x + diff.y * diff.y);
	angles.y = (atan(diff.y / diff.x) * 57.295779513082f);
	angles.x = (atan(diff.z / hypotenuse) * 57.295779513082f);
	angles.z = 0.0f;
	if (diff.x >= 0.0) angles.y += 180.0f;
	ClampAngles(&angles);
	return angles;
}


void LegitBot::ClampAngles(Vector * angles)
{
	if (angles->x > 89.f)
		angles->x = 89.f;
	else if (angles->x < -89.f)
		angles->x = -89.f;

	while(angles->y > 180.f)
		angles->y -= 360.f;
	while(angles->y < -180.f)
		angles->y += 360.f;
	angles->z = 0.0f;
}

void LegitBot::ClampMovement(CUserCmd* pCmd)
{
	pCmd->forwardmove = pCmd->forwardmove > 450.0f ? 450.0f : pCmd->forwardmove;
	pCmd->forwardmove = pCmd->forwardmove < -450.0f ? -450.0f : pCmd->forwardmove;
	pCmd->forwardmove = pCmd->sidemove > 450.0f ? 450.0f : pCmd->sidemove;
	pCmd->forwardmove = pCmd->sidemove < -450.0f ? -450.0f : pCmd->sidemove;
}

Player * LegitBot::GetClosestPlayerToCrosshair(float * distance = 0)
{
	float lowestDistance = 1000.0f;
	Player* bestCandidate = 0;
	int ScreenWidth = 0, ScreenHeight = 0;
	Vector Crosshair;

	Player* tmp = 0;
	float tmpDist = 1000.0f;
	Vector tmp3DPoint, tmpScreenPoint;

	Player* LocalPlayer = Enties->GetClientEntity(Engie->GetLocalPlayer());

	Engie->GetScreenSize(&ScreenWidth, &ScreenHeight);
	Crosshair.x = 0;
	Crosshair.y = 0;
	for (int i = 1; i < 64; i++)
	{
		tmp = Enties->GetClientEntity(i);
		if (tmp && tmp != LocalPlayer)
		{
			int myTeam = LocalPlayer->getTeam();
			int otherTeam = tmp->getTeam();
			if (myTeam != otherTeam)
			{
				tmp3DPoint = GetPointToAimAt(tmp);
				if (WorldToScreen(&tmp3DPoint, &tmpScreenPoint))
				{
					tmpDist = GetDistance(Crosshair, tmpScreenPoint);
					if (tmpDist < lowestDistance)
					{
						lowestDistance = tmpDist;
						bestCandidate = tmp;
					}
				}
			}
		}
	}
	*distance = lowestDistance;
	return bestCandidate;
	/*
	Loop through every player
	Use GetPointToAimAt on each one
	Get W2S of each person, and compare it by storing the lowest value and a pointer to the player in a var
	Return the distance in the distance pointer
	*/
}

Vector LegitBot::GetPointToAimAt(Player* target)
{
	/*
	If closest bone isn't the head/neck, up the chances of aimbotting
	Don't forget to take the aimpunch into account
	*/

	return target->getBonePosition(8);
}

LegitBot::~LegitBot()
{
}
