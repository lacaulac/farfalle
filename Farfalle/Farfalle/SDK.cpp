#include "stdafx.h"
#include "SDK.h"

FORCEINLINE float lerpF(float par1, float par2, float lerp=0.5f)
{
	//return (par1 + par2) / 2;
	return par1 * (lerp)+par2 * (1 - lerp);
}

bool WorldToScreen(Vector* input, Vector* output)
{
	matrix3x4& w2sMatrix = Engie->GetW2SMatrix();
	int ScreenWidth = 0, ScreenHeight = 0;
	Engie->GetScreenSize(&ScreenWidth, &ScreenHeight);
	float w = w2sMatrix[3][0] * input->x + w2sMatrix[3][1] * input->y + w2sMatrix[3][2] * input->z + w2sMatrix[3][3];
	output->z = 0.0f;
	if (w > 0.01)
	{
		float inversew = 1 / w;
		output->x = (ScreenWidth / 2) + (0.5 * ((w2sMatrix[0][0] * input->x + w2sMatrix[0][1] * input->y + w2sMatrix[0][2] * input->z + w2sMatrix[0][3]) * inversew) * ScreenWidth + 0.5);
		output->y = (ScreenHeight / 2) - (0.5 * ((w2sMatrix[1][0] * input->x + w2sMatrix[1][1] * input->y + w2sMatrix[1][2] * input->z + w2sMatrix[1][3]) * inversew) * ScreenHeight + 0.5);
		return true;
	}
	return false;
}

Vector LerpAngles(Vector par1, Vector par2, float lerp)
{
	Vector result;
	result.x = lerpF(par1.x, par2.x, lerp);
	result.z = 0.0f;
	float firstTry = abs(par2.y - par1.y); //Diff between angles
	if (firstTry <= 180)
	{
		result.y = lerpF(par1.y, par2.y, lerp);
	}
	else if(firstTry > 180)
	{
		float secondTry = abs((par2.y - 360.0f) - par1.y);
		if (secondTry < firstTry)
		{
			result.y = lerpF(par1.y, par2.y, lerp) + 360.0f;
		}
		else
		{
			secondTry = abs((par2.y + 360.0f) - par1.y);
			if (secondTry < firstTry)
			{
				result.y = lerpF(par1.y, par2.y, lerp) - 360.0f;
			}
		}
	}
	return result;
}

float randomFloat()
{
	return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

int randomInt(int min, int max)
{
	return (rand() % max) + min;
}
