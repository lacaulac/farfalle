#pragma once

#include "SDK.h"
#include "ConfigManager.h"

class LegitBot
{
private:
	static int AATicks; //Kind of shouldChockePacket | 1 if we just started AAing (chocked packet), 0 if second time (unchocked) or not AAing
public:
	LegitBot();
	//Initializes the class members
	static void InitLegitBot();
	//Does the aimbotting
	static void DoAimbot(CUserCmd* pCmd);
	//Does the legit AA
	static void DoLegitAA(CUserCmd* pCmd, Vector oldView);
	//Does the triggerbotting
	static void DoTrigger(CUserCmd* pCmd);
	//Compensate the recoil for aiming at a specific point using the aimpunch value (Player::getAimPunchAngles())
	static Vector CompensateRecoil(Vector pointToAccess, Vector aimPunch);
	//Calculates the angle between two points
	static Vector CalcAngles(Vector src, Vector dst);
	//Clamps the angles -> No untrusted
	static void ClampAngles(Vector* angles);
	//Clamps the movement -> No untrusted
	static void ClampMovement(CUserCmd* pCmd);
	//Gets the nearest to crosshair player (by the pos of head)
	static Player* GetClosestPlayerToCrosshair(float * distance);
	//Gets the point to aim at, duh
	static Vector GetPointToAimAt(Player* target);

	~LegitBot();
};